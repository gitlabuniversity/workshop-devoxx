
## Un autre projet

Nous allons utiliser un autre projet demo `web-api` pour découvrir les fonctionnalités autour de la gestion de conteneurs.

### Packaging Docker

* forker le projet [web-api](https://gitlab.com/gitlabuniversity/web-api) via le bouton `Fork`
* ajouter un job `🐳_build-image` pour générer une image Docker et pour la publier dans la [container registry](https://docs.gitlab.com/ee/user/packages/container_registry/) du projet
    * la version de l'image doit être le sha court du commit ([astuce](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html))
    * une manière simple est d'utiliser [kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html), cela évite de faire du *Docker-in-Docker* ou *DinD*
    * 💡 [snippet exemple](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519503) (remplacer les $****)
* commiter sur la branche `main`

```
✅ Le pipeline s'exécute correctement
✅ Dans le menu Packages and registries, l'image apparait dans la Container Registry avec comme version le sha court du commit
```

Il n'est peut pas nécessaire de publier une image pour chaque commit.

* créer une branche (sans merge request) `no-docker`
* modifier le `.gitlab-ci.yml` pour que le job `🐳_build-image` ne s'exécute que sur la branche `main` et sur les merge request
    * 💡 [astuce](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
* commiter sur la branche

```
✅ Le pipeline s'exécute correctement
✅ Le job 🐳_build-image ne s'exécute pas
```

* créer maintenant la merge request de cette branche (***attention*** à pointer sur la branche `main` de **votre projet** et pas de celle du projet d'origine qui est sélectionnée par défaut)

Un pipeline se déclenche automatiquement.

```
✅ Le pipeline s'exécute correctement
✅ Le job 🐳_build-image s'exécute
```

### Tester une API directement depuis la pipeline

GitLab fournit le mécanisme de [services](https://docs.gitlab.com/ee/ci/services/) pour faire tourner un container en parallèle du container du job. 
Cela permet de pouvoir utiliser des services annexes comme une BDD lors d'une série de tests.

On peut aussi utiliser ce mécanisme pour déployer un container créé lors du job `🐳_build-image` et ainsi faire des tests sur les API exposés par notre application.

* repasser sur la branche `main`
* modifier le `.gitlab-ci.yml` et ajouter un job `🛂_test-api` qui s'exécute **après** le job `🐳_build-image`. 
    * Ce job utilise via un *service* l'image construite à l'étape précédente.
    * Ce job appelle plusieurs fois l'url `http://webapp:8080/api/hello`       
    * [exemple](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519504)
* commiter

```
✅ Le pipeline s'exécute correctement
```

### Contrôle de sécurité

L'utilisation de dépendances open-sources, la complexification de la construction des applications font que les vecteurs de failles de sécurité sont de plus en plus nombreux. Pour contrôler cela, il existe de nombreux analyzers sur le marché. GitLab fournit des [jobs CI](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks) avec les outils prépackagés.

A partir de la branche `main`:
* créer une branche `check-security` et sa MR associée (***attention*** à pointer sur la branche `main` de votre projet et pas de celle du projet d'origine qui est sélectionnée par défaut)
* à partir de cette branche, modifier le `.gitlab-ci.yml` afin d'activer les jobs d'analyze SAST **à partir des templates fournis par GitLab**
    * pour l'analyse SAST
    * pour la détection de secrets
    * pour l'analyse de container
* configurer les templates
    * ⚠️ ces jobs sont liés au stage *test* fourni par défaut par GitLab
    * ℹ️ certains templates sont configurés pour s'exécuter sur les branches d'autres pour les MR...
    * 🎚 le job pour le scanning des containers nécessite de paramétrer le nom de l'image Docker à scanner via une variable globale au pipeline `DOCKER_IMAGE`
    * un petit [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519505) pour vous mettre sur la voie
* commiter

```
✅ Le pipeline de la MR s'exécute correctement et sans warning
✅ 4 nouveaux jobs apparaissent dans le pipeline :
_ container_scanning
_ nodejs-scan-sast
_ secret_detection
_ semgrep-sast
✅ Le pipeline de la branche ne comprend pas les nouveaux jobs
```

*NB:* On aurait facilement pu forcer également l'exécution d'un ou plusieurs jobs sur les branches également en forcant son utilisation en surchargeant la configuration

Par exemple
```yaml
nodejs-scan-sast:
  rules:
    - if: $CI_OPEN_MERGE_REQUESTS  # Add it to a *branch* pipeline even if it's already in a merge request pipeline.
      when: always
```

Ces jobs génèrent des rapports intéressants. Il serait pratique de pouvoir avoir les informations dans le rapport de MR. Malheureusement, cette fonctionnalité n'est disponible qu'en version *Ultimate* de GitLab...
Mais il existe un contournement via le mécanisme de [notes](https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#add-a-new-comment) disponibles dans les MR et les [API](https://docs.gitlab.com/ee/api/api_resources.html) exposées par GitLab. Cela ne permet une intégration aussi avancée mais permet de remonter certaines informations dans la MR.

Pour cela, il faut ajouter un job qui parse les résultats des analyzers.

Toujours sur la branche `check-security`:
* ajouter le fichier [js](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519506) qui permet de parser les fichiers json, à la racine du repository
* En utilisant ce [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519508), modifier le `.gitlab-ci.yml` pour que les résultats soient remontés dans la MR
    * *NB* : il faut peut-être modifier les jobs existants pour que le nom des rapports générés fonctionnent avec ce nouveau job ([exemple](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519509))
    * Il faut ajouter une variable d'environnement avec un token pour que l'on puisse appeler l'API. Réutiliser le token utilisé lors de l'étape de packaging

```
✅ Le pipeline s'exécute correctement
✅ Les vulnérabilités remontent dans la MR dans une note
```

ℹ️ On utilise dans le snippet `.gitlab-ci.yml`, la notion de définition de job :
* une définition est préfixée par `.` et référencée par un `&`
    * `.my-function: &my-function``
* ces fonctions peuvent être importées dans les jobs en appelant `*my-function`
    * Les méthodes définies dans cette définition peuvent être ensuite utilisées dans le job

### Scheduled pipeline

Il peut être pratique également d'avoir des pipelines qui s'exécutent de manière périodique : par exemple le matin pour construire une image avec le cache de dépendances mis à jour pour accélérer le temps d'exécution de vos jobs (en particulier quand on fait du JS ou du Java 😇). Un autre exemple est l'utilisation de bot tel que [renovabot](https://github.com/renovatebot/renovate) / [dependabot](https://github.com/dependabot) pour scanner et détecter des potentielles nouvelles versions/failles sur des dépendances.

* depuis le menu GitLab, ajouter un [scheduled pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) qui s'exécute toutes les 5min
    * Le cron à utiliser : `0/5 * * * *`
* sauvegarder

```
✅ Le pipeline s'exécute correctement
✅ Le pipeline s'exécute correctement de nouveau 5min plus tard
```

# Utiliser ses propres runners

GitLab fournit un grand nombre de shared runners, mais dans certains cas, il peut être pratique d'avoir un *private* runner pour exécuter des jobs nécessitant un environnement d'exécution particulier, manipulant des données sensibles, ...

Il existe plusieurs méthodes pour instancier un runner, voici le chemin vers la [documentation](https://docs.gitlab.com/runner/register/index.html), faites votre choix:

- Le paragraphe est marqué en *deprecated* mais le nouveau mode de fonctionnement n'est pas encore déployé
- Le plus simple reste via Docker/Podman
    * Il faut récupérer le token pour enregistrer le runner dans le menu `Settings > CICD > Runners > Project runners`
    * Avec Docker, la commande par défaut pour initaliser le runner : `docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest`
    * Puis enregister le runner auprès de GitLab : `docker exec -it gitlab-runner gitlab-runner register`

Pour cet exercice:
* enregistrer un runner local avec le tag **local**
* modifier le `.gitlab-ci.yml` pour qu'un de vos jobs soit avec le tag **local**
* commiter

```
✅ Le runner apparaît dans la liste des project runners à l'état Active
✅ Le pipeline s'exécute correctement
✅ Le job taggé s'exécute bien sur le nouveau runner (visible dans les infos du job dans le menu de droite)
✅ Les autres jobs s'exécutent toujours sur les shared runners GitLab
```

### Utilisation de child pipelines

Dans certains cas de figures, il arrive qu'un projet nécessite de déclencher le pipeline d'un autre projet. Pour cela, GitLab propose le mécanisme des [child pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html).
Dans ce workshop, nous allons modéliser le cas où l'on souhaite déclencher le pipeline de notre projet `web-app` à chaque fois que le projet `web-api` exécute un pipeline avec succès.

Sur la branche `main` du project `web-api`:
* ajouter dans `.gitlab-ci.yml` la référence pour déclencher le pipeline de votre projet `xxx-web-app` créé précédemment comme sur le snippet [suivant](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519510)
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Depuis le pipeline de web-api, on voit bien le lien vers le projet web-app
✅ Un pipeline du projet web-app se déclenche bien et s'exécute correctement
```

# Dynamic child pipeline

Pour aller encore plus loin dans l'adaptabilité de nos pipelines, il est possible de *générer* des jobs / pipelines dynamiquement et de le déclencher (ou [trigger](https://docs.gitlab.com/ee/ci/yaml/index.html#trigger)).
Cela peut être pratique lorsque l'on souhaite, par exemple, construire une application pour différentes architectures CPU, ou dans différents niveaux de compatibilités...

Sur la branche `main` du project `web-api`:
* dans `.gitlab-ci.yml`, ajouter un job *🥸_generate-mustache* qui génère un fichier `.yml` contenant nos jobs
    * vous pouvez vous inspirer de ce [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519512) utilisant l'outil `mustache` de templating
    * ⚠️ Penser à ajouter les fichiers `data.json` & `ci.template` à la racine du projet disponibles [ici](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519511)
* ajouter un autre job *👶_child-pipelines* qui utilise le fichier précédemment généré
    * astuce par [ici](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#trigger-a-dynamic-child-pipeline)
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Un downstream job est généré
✅ Les 3 jobs générés s'exécutent correctement
```

On peut ainsi facilement, ajouter de nouveaux jobs dans un pipeline sans avoir à dupliquer les configurations yml pour chacune des nouvelles valeurs.

* Dans le fichier `data.json`, ajouter `"rebellion"` dans la liste des items
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Un downstream job est généré
✅ Il y a maintenant 4 jobs générés
```
