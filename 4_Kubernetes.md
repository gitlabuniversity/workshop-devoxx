## Jouons un peu avec k8s

Dans cette dernière étape, nous allons voir les liens possibles entre GitLab & Kubernetes.

Pour celle-ci, il vous faut installer (si ce n'est déjà fait):
- `kubectl` : [Guide](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/)
- `helm` : [Guide](https://helm.sh/docs/intro/install/)

Si vous n'avez pas de cluster en local, on vous en a préparé quelques-uns. Dites nous et on vous donnera le kubeconfig qui va bien.

Vérifier la connexion avec le cluster depuis un terminal:

```bash
export KUBECONFIG=<path_to_yaml>
kubectl get pods --all-namespaces
```

```
✅ La liste de tous les pods du cluster s'affiche correctement
```

Nous allons utiliser un autre projet demo `kub-agent` pour découvrir les fonctionnalités autour du kub-agent.
* forker le projet [kub-agent](https://gitlab.com/gitlabuniversity/experiments/demo-kub-agent) via le bouton `Fork`

### Installation du Kubernetes-agent

Afin d'intéragir simple avec un cluster, GitLab fournit un [kubernetes-agent](https://docs.gitlab.com/ee/user/clusters/agent/install/).

* En suivant la [doc](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab), installer l'agent sur votre cluster depuis le menu Infrastructure > Kubernetes et le bouton *Connect a cluster*

```
✅ L'agent apparaît dans Infrastructure > Kubernetes cluster à l'état Connecté
```

* Vérifier si le gitlab-agent est bien installé dans votre cluster

```bash
kubectl get pods -n gitlab-agent-<nom_de_l_agent>
````

```
✅ Un pod apparait avec un nom <nom_de_l_agent>-gitlab-agent-xxx-xxx
```

### Intéraction avec le cluster

Le kub-agent permet de facilement intéragir avec un cluster Kubernetes sans à avoir à fournir de KUBECONFIG en variable ou via un fichier dans la CI.
Cela limite donc les risques de fuite de configuration pour accéder à votre cluster.

Depuis l'éditeur de pipelines (CICD > Editor):
* Initialiser un `.gitlab-ci.yml``
* Ajouter le [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2527098) pour vérifier que l'on arrive bien à intéragir avec le cluster depuis la CI
  * Il y a 3 variables à renseigner dans ce snippet : le nom du groupe, du projet & de l'agent
* Commiter le fichier sur la branche `main`

```
✅ Le pipeline s'exécute correctement et dans les logs du job, la commande kubectl affiche des informations
```

Vous pouvez désormais facilement intéragir avec votre cluster pour installer/mettre à jour des applications via `kubectl` ou `helm` par exemple.

## Allons plus loin

Dans le répertoire `init` du projet, il y a 2 fichiers `yaml` pour initialiser votre cluster pour gérer proprement vos environnements avec GitLab à savoir un `ingress-controller` et le composant `external-dns` 

Dans le fichier `.gitlab-ci.yml`:
* Ajouter un job pour initialiser un `ingress-controller` et un `external-dns`
  * Utiliser ce [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2531717)
* Commiter sur la branche `main`

Vérifier que tout est ok en exécutant la commande `kubectl get pods --all-namespaces`

```
✅ Le pod external-dns-xxx-xxx est à l'état Running
```

Déployons maintenant le helm chart de démo depuis la CI

Dans le fichier `.gitlab-ci.yml`:
* Ajouter le job de déploiement via helm disponible dans la snippet [suivante](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2525463)
* Commiter sur la branche `main`

```
✅ L'application est accessible via http://${CI_PROJECT_NAME}-${CI_COMMIT_BRANCH}.devoxx.yodamad.fr
```

Maintenant, on peut avoir besoin de plusieurs environnements en parallèle.

* Créer une branche `demo` à partir de `main`
* Forcer un pipeline manuellement sur cette branche `demo`

```
✅ L'application est accessible via http://${CI_PROJECT_NAME}-${CI_COMMIT_BRANCH}.devoxx.yodamad.fr
```

Dans le menu `Deployments > Environments`, les 2 environnements apparaissent bien.

Ainsi on peut facilement avoir des environnements de démo, tests automatiquement créés et intégrés dans GitLab.

Pour aller encore plus loin, il y a la features de [Feature Flags](https://docs.gitlab.com/ee/operations/feature_flags.html).